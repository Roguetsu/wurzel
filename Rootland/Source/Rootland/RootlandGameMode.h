// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RootlandGameMode.generated.h"

UCLASS(minimalapi)
class ARootlandGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARootlandGameMode();
};



